import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { map} from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { Router, RouterLinkActive} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  private adminUrl = "http://localhost:3000/admins";
  // private adminUrl = "https://192.168.0.140:49344/api/account/login";
  private extractData(res: Response){
    let body = res;
    return body ||{}
  }

  constructor(private http: HttpClient) { }

  getAdmins():Observable<any>{
    return this.http.get(`${this.adminUrl}`);
  }

  getAdmin(id:number):Observable<any>{
    return this.http.get(`${this.adminUrl}/${id}`)
  }

  creatAdmin(admin: any):Observable<any>{
    return this.http.post(`${this.adminUrl}`, admin)
  }

  updateAdmin(id:number, admin:any):Observable<any>{
    return this.http.put(`${this.adminUrl}/${id}`, admin)
  }

  deleteAdmin(id:number):Observable<any>{
    return this.http.delete(`${this.adminUrl}/${id}`)
  }

  //
  // login(admin:any):Observable<any>{
  //   return this.http.post(`${this.adminUrl}`, admin);
  // }

}

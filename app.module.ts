import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './home/products/products.component';
import { LoginComponent } from './login/login.component';

//import http
import { HttpClientModule } from '@angular/common/http';

//Material
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, } from '@angular/material';
import { MatIconModule} from '@angular/material';
import { MatInputModule} from '@angular/material/input';

// import {Filterizr} from './modules/Filterizr';
// import { RawOptionsCallbacks } from './../assets/plugins/filterizr/FilterizrOptions/defaultOptions'; 
// import { Filter } from './types/index'; 
// import { RawOptions } from './types/interfaces/';
//Routeing
import { RouterModule, Routes } from '@angular/router';
import { OrdersComponent } from './home/orders/orders.component';
import { UserComponent } from './home/user/user.component';
import { RolesComponent } from './home/roles/roles.component';
import { UserGroupComponent } from './home/user-group/user-group.component';
import { LibraryComponent } from './home/library/library.component'
import { SearchPipe } from './search.pipe';

//import module
import * as productModule from './../app/home/products/products.module';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'home',
    component: HomeComponent,
    children: [
      {
        path: 'products',
        // redirectTo: '/app-products',
        component: ProductsComponent
      },
      {
        path: 'orders',
        component: OrdersComponent
      },
      {
        path: 'user',
        component: UserComponent
      },
      {
        path: 'user-group',
        component: UserGroupComponent
      },
      {
        path: 'roles',
        component: RolesComponent
      },
      {
        path: 'library',
        component: LibraryComponent
      }
    ]
  }
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProductsComponent,
    LoginComponent,
    OrdersComponent,
    UserComponent,
    RolesComponent,
    UserGroupComponent,
    LibraryComponent,
    SearchPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule, 
    MatButtonModule, 
    MatCheckboxModule,
    MatIconModule,
    MatInputModule,   
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

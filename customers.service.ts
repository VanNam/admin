import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CustomersService {
  private customerUrl = "http://localhost:3200/customers";

  constructor(private http: HttpClient) { }

  getCustomers():Observable<any>{
    return this.http.get(`${this.customerUrl}`);
  }

  getCustomer(id:number):Observable<any>{
    return this.http.get(`${this.customerUrl}/${id}`);
  }

  

}

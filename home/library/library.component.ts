import { Component, OnInit } from '@angular/core';
import { Library} from './../../model/library';
import { LibraryService} from './library.service';

@Component({
  selector: 'library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.scss']
})
export class LibraryComponent implements OnInit {

  constructor(private librarySer: LibraryService) { }

  ngOnInit() {
  }

}

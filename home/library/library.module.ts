import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageComponent } from './home/library/image/image.component';



@NgModule({
  declarations: [ImageComponent],
  imports: [
    CommonModule
  ]
})
export class LibraryModule { }

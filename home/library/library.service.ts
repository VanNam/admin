import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LibraryService {
  private libraryUrl = "http://localhost:4800/Library"
  private extractData(res: Response){
    let body = res;
    return body ||{}
  }

  constructor(private http: HttpClient) { }

  getLibrary():Observable<any>{
    return this.http.get(`${this.libraryUrl}`);
  }

  editLibarary(library: any):Observable<any>{
    return this.http.put(`${this.libraryUrl}`, library);
  }

  deleteLibrary():Observable<any>{
    return this.http.delete(`${this.libraryUrl}`);
  }

}

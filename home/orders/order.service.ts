import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable} from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders(
  { 
     'Authorization': 'Your Token',
     'Content-Type': 'application/json'
  })
}
@Injectable({
  providedIn: 'root'
})


export class OrderService {
  private orderUrl = "http://localhost:4600/Order";
  private extractData(res: Response) {
    let body = res;
    return body || {}
  }
 

  constructor(private http: HttpClient) { }

  getOrders():Observable<any>{
    return this.http.get(`${this.orderUrl}`);
  }

  // getOrdersName(customerName: String):Observable<any>{
  //   return this.http.get()
  // }

  getOrder(id:number):Observable<any>{
    return this.http.get(`${this.orderUrl}/${id}`);
  }

  addOrder(order: any):Observable<any>{
    return this.http.post(this.orderUrl, JSON.stringify(order), httpOptions);
  }

  editOrder(id:number, order:any):Observable<any>{
    return this.http.put(`${this.orderUrl}/${id}`, order);
  }

  deleteOrder(id:number):Observable<any>{
    return this.http.delete(`${this.orderUrl}/${id}`);
  }
  
}

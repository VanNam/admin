import { Component, OnInit } from '@angular/core';
import { OrderService } from './order.service';
import { Order } from './../../model/orders';


@Component({
  selector: 'orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  order: any;
  Id: String;
  Name: String;
  Address: String;
  Email: String;
  Mobile: String;
  Message: String;
  Date: String;
  By: String;
  payment: String;
  sta: String;


  listOrder: any[];

  constructor(private orderSer: OrderService) { }

  ngOnInit() {
    this.orderSer.getOrders().subscribe(data => {
      this.listOrder = data;
      console.log('order', this.listOrder);

    })
  }

  SaveOrder() {
    let newOrder = {
      order_Id: this.Id,
      customerName: this.Name,
      customerAddress: this.Address,
      customerEmail: this.Email,
      customerMobile: this.Mobile,
      customerMessage: this.Message,
      createDate: this.Date,
      paymentStatus: this.payment,
      status: this.sta
    }
    if (this.Id === '' || this.Name === '' || this.Address === '' || this.Email === '' || this.Mobile === '' || this.Message === '' || this.Date === '' || this.payment === '' || this.sta === '') {
      alert('Bạn cần nhập đủ các thông tin đơn hàng');
    }
    else {
      alert('Thêm thành công')
      this.orderSer.addOrder(newOrder).subscribe(data => {
        newOrder = data;
        this.orderSer.getOrders().subscribe(data => {
          console.log('newData', data);
          this.listOrder = data
        })
      })

    }

  }

  //click row
  rowData = {
    id: null,
    order_Id: '',
    customerName: '',
    customerAddress: '',
    customerEmail: '',
    customerMobile: '',
    customerMessage: '',
    createDate: '',
    createBy: '',
    paymentStatus: '',
    status: '',
  }
  RowClick(orders: any, i: number) {
    this.rowData = orders;
    // this.rowData.id = i;
    console.log('id', this.rowData.id);
  }

  //click edit
  EditOrder(idEdit: number) {
    this.orderSer.editOrder(this.rowData.id, this.rowData).subscribe(data => {
      console.log('edit', this.rowData);
      this.orderSer.getOrders().subscribe(data1 => {
        console.log('new edit', data1);
      })
    })
  }

  //click delete
  DeleteOrder(idDelete: number) {
    // idDelete = this.rowData.id
    this.orderSer.deleteOrder(this.rowData.id).subscribe(data => {
      console.log('delete', this.rowData.id);
      this.orderSer.getOrders().subscribe(data => {
        console.log('new', data);

      })

    })
  }

  // chức năng tìm kiếm 
  searchName: String;
  searchListOrder: Order[] = [];
  customerName: any;
  Search() {
    if (this.customerName != '') {
      this.listOrder = this.listOrder.filter(res => {
        return res.customerName.toLocaleLowerCase().match(this.customerName.toLocaleLowerCase());
      })

    }
    else {
      this.orderSer.getOrders().subscribe(data => {
        this.listOrder = data
      })
    }


  }

}

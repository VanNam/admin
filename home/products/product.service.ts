import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private productUrl = "http://localhost:3100/Products";

  constructor(private http: HttpClient) { }

  getProducts():Observable<any>{
    return this.http.get(`${this.productUrl}`);
  }

  getProduct(id:number):Observable<any>{
    return this.http.get(`${this.productUrl}/${id}`)
  }

  addProduct(product:any):Observable<any>{
    return this.http.post(`${this.productUrl}`, product);
  }

  deleteProduct(id:number):Observable<any>{
    return this.http.delete(`${this.productUrl}/${id}`);
  }

  editProduct(id:number, product:any):Observable<any>{
    return this.http.put(`${this.productUrl}/${id}`, product);
  }

}

import { Component, OnInit } from '@angular/core';
import { ProductService} from './product.service';
import { Product} from './../../model/products';

@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  listProduct: any;

  constructor(private productSer: ProductService) { }

  ngOnInit() {
    this.productSer.getProducts().subscribe(data => {
      this.listProduct = data;
      console.log('listProduct', this.listProduct);
      
    })
  }

}

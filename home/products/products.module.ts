import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibraryComponent } from './home/library/library.component';



@NgModule({
  declarations: [LibraryComponent],
  imports: [
    CommonModule
  ]
})
export class ProductsModule { }

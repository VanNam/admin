import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  private roleUrl= "http://localhost:3500/Role";

  constructor(private http: HttpClient) { }

  getRoles():Observable<any>{
    return this.http.get(`${this.roleUrl}`);
  }

  addRole(role: any):Observable<any>{
    return this.http.post(`${this.roleUrl}`, role);
  }
}

import { Component, OnInit } from '@angular/core';
import { RoleService} from './role.service';
import { Role} from './../../model/roles';

@Component({
  selector: 'roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {

  listRole: any;

  constructor(private RoleSer: RoleService) { }

  ngOnInit() {
    this.RoleSer.getRoles().subscribe(data => {
      this.listRole = data;
      console.log('listRole', this.listRole);
      
    })
  }

  name: String;
  des:String;
  saveRole(){
    let newRole = {
      nameRole: this.name,
      descriptiong: this.des
    }
    if(this.name == '' || this.des == ''){
      alert('Bạn cần nhập đủ thông tin')
    }
    else{
      this.RoleSer.addRole(newRole).subscribe(data => {
        newRole = data;
      })
    }
  }


  //click row
  rowData = {
    id: null,
    order_Id: '',
    customerName: '',
    customerAddress: '',
    customerEmail: '',
    customerMobile: '',
    customerMessage: '',
    createDate: '',
    createBy: '',
    paymentStatus: '',
    status: '',
  }
  RowClick(orders: any, i: number) {
    this.rowData = orders;
    // this.rowData.id = i;
    console.log('id', this.rowData.id);
  }

}

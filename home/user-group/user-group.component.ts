import { Component, OnInit } from '@angular/core';
import { UserGroupService} from './user-group.service';
import { userGroup} from './../../model/user-groups';

@Component({
  selector: 'user-group',
  templateUrl: './user-group.component.html',
  styleUrls: ['./user-group.component.scss']
})
export class UserGroupComponent implements OnInit {
  listUserGroup: any;
  groupName:String;

  constructor(private userGroupSer: UserGroupService) { }

  ngOnInit() {
    this.userGroupSer.getUserGroups().subscribe(data => {
      this.listUserGroup = data;
      console.log('userGroup', this.listUserGroup);
      
    })
  }

  saveGroupUser(){

  }

  //rowclick
  listRowClick = {
    id: null,
    NameGroup: '',
    Description: ''
  };
  RowClick(list:any, i: any){
    this.listRowClick = list;
  }

  //edit Group
  EditGroup(id:number){
    this.userGroupSer.editUserGroup(this.listRowClick.id, this.listRowClick).subscribe(data => {
      alert('Bạn đã sửa `${this.listRowClick.NameGroup}`');
      this.userGroupSer.getUserGroups().subscribe(data1 => {
        this.listUserGroup = data1;
      })
    })
  }


  DeleteGroup(id:number){
    this.userGroupSer.deleteUserGroup(this.listRowClick.id).subscribe(data => {
      alert('Bạn đã xóa  `${this.listRowClick.NameGroup}`');
      this.userGroupSer.getUserGroups().subscribe(data1 => {
        this.listUserGroup = data1;
      })
    }) 
  }

}

import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'
import { Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserGroupService {
  private userGroupUrl = "http://localhost:3400/User-groups";

  private exTractData(res: Response){
    let body = res;
    return body || {}
  } 

  constructor(private http: HttpClient) { }

  getUserGroups():Observable<any>{
    return this.http.get(`${this.userGroupUrl}`);
  }

  getUserGroup(id:number):Observable<any>{
    return this.http.get(`${this.userGroupUrl}/${id}`);
  }

  deleteUserGroup(id:number):Observable<any>{
    return this.http.delete(`${this.userGroupUrl}/${id}`);
  }

  editUserGroup(id:number, userGroup: any):Observable<any>{
    return this.http.put(`${this.userGroupUrl}/${id}`, userGroup);
  }

}

import { Component, OnInit } from '@angular/core';
import { User } from './../../model/users'
import { UserService } from './user.service';

@Component({
  selector: 'user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  constructor(private userSer: UserService) { }

  listUsers: any;
  id: number;
  FullName: String;
  Address: String;
  Email: String;
  Mobile: String;
  user: String;
  pass: String; 

  ngOnInit() {
    this.userSer.getUsers().subscribe(data => {
      this.listUsers = data;
    })
  }

  saveUser() {
    let newUser = {
      idUer: this.id,
      FullNameUer: this.FullName,
      AddressUer: this.Address,
      EmailUer: this.Email,
      MobileUer: this.Mobile,
      userUer: this.user,
      passUer: this.pass
    }
    this.userSer.addUser(newUser).subscribe(data => {
      console.log('newUser', newUser);
      this.userSer.getUsers().subscribe(data1 => {
        this.listUsers = data1;
      })
    })
  }


  //click row
  UserClickRow: User = {
    id: null,
    FullName: '',
    Address: '',
    Email: '',
    Mobile: '',
    user: '',
    pass: ''
  }
  RowClick(user: any, i: number) {
    this.UserClickRow = user;
  }

  EditUer(id:number){
    this.userSer.editUser(this.UserClickRow.id, this.UserClickRow).subscribe(data => {
      alert('Bạn đã sửa thành công');
      this.userSer.getUsers().subscribe(data1 => {
        this.listUsers = data1;
        console.log('Sau khi edit', data1);
        
      })
    })
  }

  DeleteUer(id: number) {
    this.userSer.deleteUser(this.UserClickRow.id).subscribe(data => {
      alert('bạn đã xóa user');
      this.userSer.getUsers().subscribe(data1 => {
        this.listUsers = data1;
      })
    })
  }

}

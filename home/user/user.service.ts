import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable} from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userUrl = "http://localhost:3300/User";

  constructor(private http: HttpClient) { }

  getUsers():Observable<any>{
    return this.http.get(`${this.userUrl}`);
  }

  getUser(id:number):Observable<any>{
    return this.http.get(`${this.userUrl}/${id}`);
  }

  addUser(user:any):Observable<any>{
    return this.http.post(`${this.userUrl}`, user);
  }

  deleteUser(id:number):Observable<any>{
    return this.http.delete(`${this.userUrl}/${id}`);
  }

  editUser(id:number, user:any):Observable<any>{
    return this.http.put(`${this.userUrl}/${id}`, user);
  }

}

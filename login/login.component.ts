import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../admin.service';
import { Admin } from '../model/admins';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  admin: Admin;
  Admin = {
    userAd: '',
    passAd: ''
  }

  constructor(private adminSer: AdminService, private router: Router) { }

  ngOnInit() {
    this.adminSer.getAdmins().subscribe(data => {
      this.admin = data;
      console.log('admin', this.admin);
    })
  }

  Login(){
    this.adminSer.getAdmins().subscribe(data => {
      if(data){
        data.forEach(item => {
          if(item.user === this.Admin.userAd && item.pass === this.Admin.passAd){
            this.router.navigate(['home/products']);
          }
          else{
            // alert('Mời nhập đúng tài khoản để tiếp tục quản trị')
          }
        });
      }
    })
  }

  // Login(){
  //   let newUser = {
  //     user: this.Admin.userAd,
  //     pass: this.Admin.passAd
  //   }
  //   this.adminSer.login(newUser).subscribe(data => {
  //     console.log('newUser', data);
  //     newUser = data;
  //   })
  // }

}

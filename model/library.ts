export class Library {
    libraryId: number;
    libName: String;
    libContent: String;
    libType: String;
    libUrl: String;
}
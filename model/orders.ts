export class Order {
    id:number;
    order_Id: String;
    customerName: String;
    customerAddress:String;
    customerEmail: string;
    customerMobile: String;
    customerMessage: String;
    createDate: String;
    createBy: String;
    paymentStatus: String;
    status: String;
}
export class Product {
    id: number;
    Name: String;
    Alias: String;
    CategoryID: String;
    Image: String;
    Price: number;
    PromotionPrice: number;
    Warranty: number;
    Descripton: String;
    Content: String;
    HomeFlag: number;
    HotFlag: number;
    ViewCount: number;
    Status: number;
    CreateDate: String;
    CreateBy: String;
    UpdatedDate: String;
    UpdateBy: String;
    Tags: String;
    Quantity: number;
}

